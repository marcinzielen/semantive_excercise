from pprint import pprint
import threading
import time
import os
import errno
import urllib2
import codecs
from bs4 import BeautifulSoup as BS
from misc_utils import get_url, get_text, get_images, get_data
from mysql_utils import *



class class_downloading_thread(threading.Thread):




    def __init__(self, orderid, url, with_images):
	threading.Thread.__init__(self)
        self.orderid = orderid        
        self.url = url
        self.with_images = with_images



    def run(self):
        print "Starting " + self.name + " " + str(self.orderid) + " " + self.url
        
        try:

	    # Get data to result dir
    	    result_dir_tmp = "./results/" + str(self.orderid) + ".tmp"
    	    result_dir_commit = "./results/" + str(self.orderid)
	    get_data(result_dir_tmp, result_dir_commit, self.url, self.with_images)

	    # Update order status in database to "Done"
    	    mysql_set_order_done(self.orderid)
	    return

	except Exception as e:

	    # Update order status in database to "Failed"
    	    print str(type(e)) + " : " + str(e)
    	    mysql_set_order_failed(self.orderid)
    	    return

