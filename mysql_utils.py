from pprint import pprint
import threading
import time
import os
import errno
import urllib2
import codecs
from bs4 import BeautifulSoup as BS
import MySQLdb




def mysql_get_connection():
    connection = MySQLdb.connect( \
	        host = "localhost", \
	        user = "semantive_user", \
	    password = "semantive_password", \
	    database = "semantive_database") 
    return connection




def mysql_delete_from_orders():

    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute(
		"delete from orders")
    connection.commit()




def mysql_create_order(url, with_images):

    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute(
		"insert into orders (id, url, with_images, status) values " \
		" ( 0, \"{}\", {}, \"Awaiting\")".format(url, with_images))
    orderid = int(connection.insert_id())
    connection.commit()
    return(orderid)




def mysql_get_status(orderid):
    
    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute( \
	        "select status from orders where id = {}".format(orderid))
        rows = cursor.fetchall()
    if len(rows) != 1:
	return None
    status = rows[0][0]
    return status




def mysql_get_awaiting_orders():
    
    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute( \
		"select id from orders where status = \"Awaiting\"")
        rows = cursor.fetchall()
    awaiting_orders = []
    for row in rows:
	awaiting_orders.append(row[0])
    return awaiting_orders




def mysql_set_order_running(orderid):

    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute(
		"update orders set status = \"Running\" " \
		"where id = {}".format(orderid))
    connection.commit()




def mysql_set_order_done(orderid):

    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute(
		"update orders set status = \"Done\" " \
		"where id = {}".format(orderid))
    connection.commit()




def mysql_set_order_failed(orderid):

    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute(
		"update orders set status = \"Failed\" " \
		"where id = {}".format(orderid))
    connection.commit()




def mysql_get_url_and_with_images(orderid):
    
    connection = mysql_get_connection()
    with connection.cursor() as cursor:
	cursor.execute( \
    		"select url, with_images from orders where id = {}".format(orderid))
	rows = cursor.fetchall()
    if len(rows) != 1:
	return None
    url = rows[0][0]
    with_images = rows[0][1]
    return url, with_images


