import unittest
from misc_utils import get_url, get_text, get_images, get_data
from mysql_utils import *
import shutil
import os
from pprint import pprint
from filecmp import dircmp



class TestGetUrl(unittest.TestCase):

    def test_get_url_1(self):
	text = get_url("https://semantive.pl/wp-content/uploads/2019/01/logo_72dpi_150px.png")
        self.assertEqual(len(text), 6545)

    def test_get_url_2(self):
	text = get_url("http://www.semantive.comm/")
        self.assertEqual(text, None)

    def test_get_url_3(self):
	text = get_url("https://pl.wikipedia.org/wiki/Test")
        self.assertEqual(len(text), 32969)



class TestGetText(unittest.TestCase):

    def test_get_text_1(self):
    	html = r'''
            <html>
                <body>
                    <b>Project:</b> DeHTML<br>
                    <b>Description</b>:<br>
                    Text
                </body>
            </html>'''
	                                                                                                                    
	text = get_text(html)
        self.assertEqual(text, u'Project: DeHTML\nDescription:\nText')

    def test_get_text_2(self):
	text = get_text("")
        self.assertEqual(text, "")



class TestGetData(unittest.TestCase):

    def setUp(self):
	shutil.rmtree("./.test_results/", ignore_errors=True)
	os.mkdir("./.test_results/")
    
    def test_get_data_1(self):
	get_data("./.test_results/1.tmp/", "./.test_results/1/", \
	    "https://pl.wikipedia.org/wiki/Test", \
	    False)	      
	dcmp = dircmp("./.test_results/1/", "./.proper_test_results/1/")
        self.assertEqual(dcmp.diff_files, [])

    def tearDown(self):
	shutil.rmtree("./.test_results/", ignore_errors=True)



class TestMySQL(unittest.TestCase):


    def setUp(self):
	mysql_delete_from_orders()
	

    def test_get_awaiting_orders(self):
	orderid1 = mysql_create_order("http://www.semantive.com/", True)
	orderid2 = mysql_create_order("http://www.semantive.com/", True)
	awaiting_orders = mysql_get_awaiting_orders()
        self.assertEqual([orderid1, orderid2], awaiting_orders)


    def test_get_status(self):
	orderid1 = mysql_create_order("http://www.semantive.com/", True)
	status = mysql_get_status(orderid1)
        self.assertEqual("Awaiting", status)


    def test_set_status_running(self):
	orderid1 = mysql_create_order("http://www.semantive.com/", True)
	mysql_set_order_running(orderid1)
	status = mysql_get_status(orderid1)
        self.assertEqual("Running", status)


    def tearDown(self):
	mysql_delete_from_orders()



if __name__ == '__main__':
    unittest.main()
