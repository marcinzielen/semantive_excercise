create database semantive_database;
create user semantive_user identified by "semantive_password";
grant all privileges on semantive_database.* to 'semantive_user'@'%';
create table orders( id integer not null primary key auto_increment, \
		     url varchar(1000) not null, \
		     with_images boolean not null default FALSE, \
                     status varchar(20) not null default "Awaiting");
 