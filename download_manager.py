from pprint import pprint
import threading
import time
import shutil
from downloading_thread import class_downloading_thread
from mysql_utils import *


class class_download_manager(threading.Thread):




    def __init__(self):
	threading.Thread.__init__(self)
	self.running_order_handlers = {}
        print "Creating download_manager"

    	# Create -p results directory
    	try:
    	    os.mkdir( "./results/", 0755 );
    	except OSError as exc:
    	    if exc.errno != errno.EEXIST:
    	        raise
    	    pass



    def start_order(self, orderid):

	# Update order status in database to "Running"
	mysql_set_order_running(orderid)
	url, with_images = mysql_get_url_and_with_images(orderid)
	
	# Start downloading thread
        try:
	    downloading_thread = class_downloading_thread(orderid, url, with_images)
	    downloading_thread.daemon = True
	    downloading_thread.start()
	    self.running_order_handlers[orderid] = downloading_thread
	except (KeyboardInterrupt, SystemExit):
	    print '\n! Received keyboard interrupt, quitting threads.\n'	



    def process_orders(self):

	# Start awaiting orders
	awaiting_orders = mysql_get_awaiting_orders()
	for orderid in awaiting_orders:
	    self.start_order(orderid)
	    
	# Join completed orders
	completed_orders = []
	for orderid in self.running_order_handlers:
	    if not self.running_order_handlers[orderid].is_alive():
		completed_orders.append(orderid)
	for orderid in completed_orders:
    	    self.running_order_handlers[orderid].join()
	    del self.running_order_handlers[orderid]	




    def run(self):
	while True:
	    print "Loop download_manager"
	    self.process_orders()
	    time.sleep(10)
	    