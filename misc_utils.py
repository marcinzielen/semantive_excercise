from pprint import pprint
import threading
import time
import os
import errno
import urllib2
import codecs
from bs4 import BeautifulSoup as BS



def get_url(url):

    attempts = 0
    while attempts < 3:
    	try:
	    request = urllib2.Request(url)
    	    response = urllib2.urlopen(request, timeout = 5)
    	    content = response.read()
    	    return content
    	except urllib2.URLError as e:
    	    attempts += 1
    	    print "Got exception " + str(type(e)) + " : " + str(e) + " in get_url(" + url + ")"
    	except Exception as e:
    	    attempts += 1
    	    print "Got exception " + str(type(e)) + " : " + str(e) + " in get_url(" + url + ")"
    return None



def get_text(html_content):
    
    # Parse html
    parsed_html = BS(html_content, features="html.parser")

    # Kill all script and style elements
    for script in parsed_html(["script", "style"]):
	script.extract()    # rip it out

    # Get text
    text = parsed_html.get_text()

    # Break into lines and remove leading and trailing space on each
    lines = (line.strip() for line in text.splitlines())

    # Break multi-headlines into a line each
    chunks = (phrase.strip() for line in lines for phrase in line.split("  "))

    # Drop blank lines
    text = '\n'.join(chunk for chunk in chunks if chunk)

    return text




def get_images(html_content):

    # Parse html
    parsed_html = BS(html_content, features="html.parser")

    images = {}
	
    for imgtag in parsed_html.find_all('img'):
	source = imgtag["src"]
	if source[0:2] == "//":
	    source = "http:" + source
	images[len(images) + 1] = source
	
    return images




def get_data(result_dir_tmp, result_dir_commit, url, with_images):


    # Create temporary results dir
    os.mkdir(result_dir_tmp, 0755);
            

    # Create file order.txt
    print("Create file order.txt")
    with open(result_dir_tmp + "/order.txt", "w") as f:
	f.write(url + "\n") 
	f.write(str(with_images) + "\n") 


    # Get url
    print("Get url: " + url)
    html_content = get_url(url)


    # Create file text.txt
    print("Create file text.txt")
    text = get_text(html_content)
    with codecs.open(result_dir_tmp + "/text.txt", "w", "utf-8") as f:
        f.write(text)


    # Create file images.txt
    print("Create file images.txt")
    images = get_images(html_content)
    with codecs.open(result_dir_tmp + "/images.txt", "w", "utf-8") as f:
        f.write(str(len(images)) + "\n")
        for imageid in images:
	    line = str(imageid) + "\t" + str(images[imageid]) + "\n"
	    f.write(line)                                          


    # Get images
    print("Get images")
    if with_images == True:

	# Get images
	for imageid in images:
	    source = images[imageid]
            print("Get file >" + str(imageid) + " " + source + "<")
	    
	    # Create file img_{counter}.txt
            content = get_url(source)
            # Skip if image not found
            if content == None:
                continue
    	    with open(result_dir_tmp + "/img_" + str(imageid), "w") as f:
        	f.write(content)                                          


    # Commit downloading with status Done
    os.rename(result_dir_tmp, result_dir_commit)


