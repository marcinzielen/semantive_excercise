from flask import Flask, jsonify, abort, make_response, request, send_file
from pprint import pprint
import threading
import time
from download_manager import class_download_manager
from mysql_utils import *



app = Flask(__name__)




@app.route('/todo/api/v1.0/orders', methods=['POST'])
def create_order():

    # Parse params
    if not request.json or not 'url' in request.json:
        abort(400)
    url = request.json['url']

    with_images = False
    if 'with_images' in request.json and request.json['with_images'] == 'True':
	with_images = True
	
    # Create order
    orderid = mysql_create_order(url, with_images)
    return jsonify({'orderid': orderid}), 201





@app.route('/todo/api/v1.0/status/<int:orderid>', methods=['GET'])
def get_status(orderid):
    status = mysql_get_status(orderid)
    if status == None:
        abort(404)
    return jsonify({"status": status})





@app.route('/todo/api/v1.0/text/<int:orderid>', methods=['GET'])
def get_text(orderid):

    textfile = "./results/" + str(orderid) + "/text.txt"
    if not os.path.isfile(textfile):
        abort(404)
    return send_file(textfile, mimetype='text/plain')





@app.route('/todo/api/v1.0/image/<int:orderid>/<int:imageid>', methods=['GET'])
def get_image(orderid, imageid):

    imagefile = "./results/" + str(orderid) + "/img_" + str(imageid)
    if not os.path.isfile(imagefile):
        abort(404)
    return send_file(imagefile, mimetype='image/gif')





if __name__ == '__main__':


    try:
	global_download_manager_thread = class_download_manager()
	global_download_manager_thread.daemon = True
	global_download_manager_thread.start()
    except (KeyboardInterrupt, SystemExit):
	print '\n! Received keyboard interrupt, quitting threads.\n'	
    app.run(debug=False)


